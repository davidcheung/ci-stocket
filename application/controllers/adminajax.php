<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class adminAjax extends CI_Controller
{
	public $json_output = array(
		
		"data"=>array(
			"result"=>"success"
		)
	);

	function __construct(){
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('');
		}
		$this->userdata['user_id']	= $this->tank_auth->get_user_id();
		$this->userdata['username']	= $this->tank_auth->get_username();
		$this->userdata['profile'] = $this->profile->getProfile( $this->userdata['user_id'] );
		$this->userdata['current_page']= $this->uri->segment(1);
		if ( !$this->userdata['profile']['is_admin'] == 1 ){
			redirect('');
		}

	}

	protected function setOutput($key,$value){
		$this->json_output['data'][$key] =$value;
	}
	protected function setData( $data ){
		$this->json_output['data']['data'] =$data;
	}

	function add_item(){
		$res = $this->item->add_item($_POST);

		$this->setData( array(
			"res"=>$res,
			"posted"=>$_POST
		) );
		$this->load->view('json', $this->json_output );
	}

	function get_items(){
		$res = $this->item->get_items($_POST['image_id']);
		$this->setData( array(
			"res"=>$res
		) );
		$this->load->view('json', $this->json_output );
	}

	function change_storage_status(){
		if (!$_POST['storage_id']){
			throw new Exception("Storage ID expected");
		}else if ( !isset($_POST['status'])){
			throw new Exception("Status expected");
		}
		$rowCount = $this->storage->change_storage_status($_POST['storage_id'],$_POST['status']);
		$this->setData( $rowCount );
		$this->load->view('json', $this->json_output );
	}

}
?>