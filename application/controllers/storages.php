<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Storages extends CI_Controller
{
	protected $userdata;
	function __construct()
	{
		parent::__construct();
		$this->userdata['user_id']	= $this->tank_auth->get_user_id();
		$this->userdata['username']	= $this->tank_auth->get_username();
		$this->userdata['profile'] = $this->profile->getProfile( $this->userdata['user_id'] );
		$this->userdata['current_page']= $this->uri->segment(1);
		
	}

	function __destruct(){
		
	}

	function index( $id = 0 ){
		redirect('');
	}

	function storage_lookup( $hash ){
		$storage_id = $this->storage->unhash($hash);
		//get items
		$items = $this->item->getItemsDetailsByStorageId($storage_id );

		$this->load->view('layouts/header', $this->userdata);
		$this->load->view('app/storage-items', array(
			"storage_id"=>$storage_id,
			"items"=> $items,
			"categories"=>$this->category->getCategories()
		));
		$this->load->view('layouts/footer');
	}

	function storage_management(){
		$this->load->view('layouts/header', $this->userdata);
		$data = array(
			"storage" => $this->storage->getStoragesByUser( $this->userdata['user_id'])
		);
		$this->load->view('app/dashboard', $data );
		$this->load->view('layouts/footer');
	}

	function add(){
		if ( !$_POST ){
			$this->load->view('app/storage-add');
			return;
		}
		$id = $this->storage->create( $this->userdata['user_id'], $_POST);
		if ( $id ){
			redirect('/dashboard');
		}
	}



}