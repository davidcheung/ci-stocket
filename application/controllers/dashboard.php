<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	protected $userdata;
	function __construct()
	{
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('');
		}
		$this->userdata['user_id']	= $this->tank_auth->get_user_id();
		$this->userdata['username']	= $this->tank_auth->get_username();
		$this->userdata['profile'] = $this->profile->getProfile( $this->userdata['user_id'] );
		$this->userdata['current_page']= $this->uri->segment(1);
		$this->load->view('layouts/header', $this->userdata);
	}

	function __destruct(){
		$this->load->view('layouts/footer');
	}

	function index()
	{
		$this->dashboard();
	}

	function dashboard(){
		$this->load->model('app/storage');
		$data = array(
			"storage" => $this->storage->getStoragesByUser( $this->userdata['user_id'])
		);
		$this->load->view('app/dashboard', $data );
	}

}

