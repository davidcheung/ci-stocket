<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Picture extends CI_Controller
{

	protected $hashObj;

	function __construct()
	{
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('');
		}
		$this->userdata['user_id']	= $this->tank_auth->get_user_id();
		$this->userdata['username']	= $this->tank_auth->get_username();
		$this->userdata['profile'] = $this->profile->getProfile( $this->userdata['user_id'] );
		$this->userdata['current_page']= $this->uri->segment(1);
		$this->hashObj = new Hashids('picture',10,'abcdefghijklmnopqrstuvwxyz1234567890');
	}

	function __destruct(){
		
	}

	function index(){
		$showInstruction = false;
		if ( $showInstruction ){
			$this->showInstruction();
		}else{
			$this->takePicture();
		}
	}

	function takePicture(){
		$this->load->view('layouts/header', $this->userdata);
		$this->load->model('app/storage');
		$data = array(
			"storage" => $this->storage->getStoragesByUser( $this->userdata['user_id'])
		);
		$this->load->view('app/takepicture',$data);
		$this->load->view('layouts/footer');
	}

	function upload(){
		// $config['upload_path'] = './uploads/';
  //       $config['allowed_types'] = 'gif|jpg|png|doc|txt';
  //       $config['max_size'] = 1024 * 8;
  //       $config['encrypt_name'] = TRUE;
		// $this->load->library('upload');
		// $this->upload->initialize($config);
		// if (!$this->upload->do_upload()){
		// 	throw new Exception( "fuck off");
		// }else{
		// 	$uploaded_data =  $this->upload->data();
		// }
		// if ( $_FILES['uploads'] ){
		// 	foreach ( $_FILES['uploads']['name'] as $k=>$v ){
		// 		move_uploaded_file( $_FILES["uploads"]["tmp_name"][$k],
  //     			"uploads/" . $_FILES["uploads"]["name"][$k]);
		// 	}
		// }
		$this->load->view( 'app/upload', array(
			"posted" => $_POST,
			"uploaded"=> $_FILES
		)  );
	}

	function ajaxUpload(){
		if ( !isset($_FILES['uploads']) ){
			//no images
			$data = array(
				"result"=>"error",
				"msg"=>"No files selected"
			);
			$this->load->view( 'json' , array("data"=>$data) );
			return;
		}else{
			//create new storage ( should i always create new storage? )
			if ( !trim($_POST['storage']) ){

			}
			else{
				//create storage
				$this->load->model('app/storage');
				$this->load->model('app/image');
				$storageid = $this->storage->create( 
					$this->userdata['user_id'], 
					array( "name"=>trim($_POST['storage']) )
				);
				$imageids = $this->image->bulkUpload($storageid, $_FILES);
				$data = array(
					"result"=>"success",
					"storage"=>$storageid,
					"count" => count($_FILES['uploads']['name']),
					"imageids"=>$imageids,
					"post"=> $_POST,
					"file"=>$_FILES
				);
				$this->load->view( 'json' , array("data"=>$data) );
			}
		}
	}



}
?>