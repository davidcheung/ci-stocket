<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller
{

	function __construct(){
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('');
		}
		$this->userdata['user_id']	= $this->tank_auth->get_user_id();
		$this->userdata['username']	= $this->tank_auth->get_username();
		$this->userdata['profile'] = $this->profile->getProfile( $this->userdata['user_id'] );
		$this->userdata['current_page']= $this->uri->segment(1);
		if ( !$this->userdata['profile']['is_admin'] == 1 ){
			redirect('');
		}

		$this->load->view('layouts/header', $this->userdata);
	}
	function __destruct(){
		$this->load->view('layouts/footer');
	}

	function index(){
		$this->adminFunctionalities();
	}

	function adminFunctionalities(){
		$this->load->view('admin/functionalities');
	}

	function pendings(){
		$pending_storages = $this->storage->getStorages();
		$data = array(
			"storage"=>$pending_storages
		);
		$this->load->view( 'admin/pendings' ,$data);
	}

	function storage_setup( $hash ){
		$id = $this->storage->unhash($hash);
		$images = $this->image->findImages( $id );
		$data = array(
			"images"=>$images,
			"storage_hash"=>$this->storage->hash($id),
			"storage"=>$this->storage->getStorageById($id)
		);
		$this->load->view( 'admin/storage_setup' ,$data);

	}

	function image_setup( $hash ){
		$id = $this->image->unhash($hash);
		$image_details = $this->image->findImageDetails( $id );
		$data = array(
			"image"=>$image_details,
			"categories"=>$this->category->getCategories()
		);
		$this->load->view( 'admin/image_edit' , $data );
	}

	function categories_management(){
		$this->category->getCategories();
		$data = array(
			"categories"=>$this->category->getCategories()
		);
		$this->load->view( 'admin/categories_management' , $data );
	}

}

?>