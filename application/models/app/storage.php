<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class storage extends CI_Model {

	public $storage_id;
	public $storage_label;
	public $storage_userid;
	protected $data;
	protected $tablename = "storage";
	protected $hashObj;


	public function __construct(){
		$this->hashObj = new Hashids('storage',10,'abcdefghijklmnopqrstuvwxyz1234567890');
	}

	public function hash($id){
		return $this->hashObj->encode($id);
	}

	public function unhash($str){
		$result = $this->hashObj->decode($str);
		return $result[0];
	}

	public function create( $userid, $posted, $status = 2 ){
		if ( !trim($posted['name']) ){
			throw new Exception( "No name");
		}else{
			//throw new Exception( print_r($posted,true) );
			$query = "INSERT INTO `".$this->tablename."`
			SET 
				`name`=". $this->customdatabase->escape($posted['name']).",
				`userid`='$userid',
				`added_on`='".date('Y-m-d H:i:s')."',
				`last_modified`='".date('Y-m-d H:i:s')."',
				`status`='$status'
			";

			$id = $this->customdatabase->insert($query);
			return $id;
		}
		
	}

	public function getStoragesByUser( $userid ){
		$query = "SELECT 
			s.*,
			(select count(1) from `images` i where i.storage_id = s.id  ) as 'image_count',
			(select count(1) from `items`  where items.storage_id = s.id  ) as 'item_count'
		FROM `{$this->tablename}` s
		WHERE
			`userid`='$userid'
			AND `status`>0
		ORDER by last_modified DESC";
		$res = $this->customdatabase->query( $query );
		foreach ( $res as $k=>$v ){
			$res[$k]['id_hash'] = $this->hash($v['id']);
		}
		return $res;
	}




	public function getStorageByStatus ( $status ){
		//admin only validation done on controller
		$query = "SELECT 
			s.*,
			(select count(1) from `images` i where i.storage_id = s.id  ) as 'image_count',
			(select count(1) from `items`  where items.storage_id = s.id  ) as 'item_count'
		FROM `{$this->tablename}` s
		WHERE
			`status`='$status'
		ORDER by last_modified DESC";
		return $this->customdatabase->query( $query );
	}
	public function getStorageById( $id ){
		//admin only validation done on controller
		$query = "SELECT 
			s.*,
			(select count(1) from `images` i where i.storage_id = s.id  ) as 'image_count',
			(select count(1) from `items`  where items.storage_id = s.id  ) as 'item_count'
		FROM `{$this->tablename}` s
		WHERE
			`id`='$id'
			AND `status`>0
		ORDER by last_modified DESC";
		$res = $this->customdatabase->query( $query );
		return $res[0];
	}


	public function getStorages( ){
		//admin only validation done on controller
		$query = "SELECT 
			s.id as 'storage_id',
			s.*,
			up.firstname,
			up.lastname,
			(select count(1) from `images` i where i.storage_id = s.id  ) as 'image_count',
			(select count(1) from `items`  where items.storage_id = s.id  ) as 'item_count'
		FROM 
			`{$this->tablename}` s
			inner join `user_profiles` up on ( s.userid = up.user_id)
		WHERE
			`status`>0
		ORDER by added_on DESC";
		$res = $this->customdatabase->query( $query );
		foreach ( $res as $k=>$v ){
			$res[$k]['id_hash'] = $this->hash($v['id']);
		}
		return $res;
	}

	public function change_storage_status( $storage_id, $status ){
		$query = "UPDATE
		`{$this->tablename}`
		SET
			`status`=".$this->customdatabase->escape($status)."
			WHERE `id`=".$this->customdatabase->escape($storage_id)."
		";
		$rowCount = $this->customdatabase->update($query);
		return $rowCount;

	}

}
?>