<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class item extends CI_Model {

	const TABLENAME = "items";
	const ITEM_FOLDER = "item-images/";
	function __construct(){
		$this->hashObj = new Hashids('storage',16,'abcdefghijklmnopqrstuvwxyz1234567890_-');
	}
	public function hash($id){
		return $this->hashObj->encode($id);
	}

	public function unhash($str){
		$result = $this->hashObj->decode($str);
		return $result[0];
	}

	function add_item($posted){
		if ( $posted['image_id'] < 1 ){
			throw new Exception( "Expecting Image ID");
		}if ( $posted['storage_id'] < 1 ){
			throw new Exception( "Expecting Storage ID");
		}

		$dataSchema = array(
			"storage_id"=>"storage_id",
			"image_id"=>"image_id",
			"name"=>"item-name",
			"category_id"=>"category_id",
			"quantity"=>"item-qty",
			"x"=>"coordinates-x",
			"y"=>"coordinates-y",
			"w"=>"coordinates-w",
			"h"=>"coordinates-h",
		);

		$params = CustomDatabase::fieldsMapping($dataSchema,$posted);
		$insert = "INSERT INTO `".self::TABLENAME."`
		set 
			".CustomDatabase::array2querystring($params)."
		";

		$id = $this->customdatabase->insert($insert);
		$item_imge = $this->createItemImage( 
			$id,
			$params['image_id'],
			$params['x'],
			$params['y'],
			$params['w'],
			$params['h']
		);
		return $id;

	}

	protected function createItemImage($item_id,$image_id,$x,$y,$w,$h){
		//no matter what input, ends with jpg
		$newPath = self::ITEM_FOLDER . $this->hash($item_id).".jpg";
		$image_details = $this->image->findImageDetails($image_id);
		$localimgpath = $this->image->buildImagePath($image_details['storage_id'],$image_details['filename'], 1);
		$pathinfo = pathinfo($localimgpath);

		if ( strtolower($pathinfo['extension']) =="jpg" ||
		strtolower($pathinfo['extension']) =="jpeg" ){
			//JPG handling
			$src = imagecreatefromjpeg($localimgpath);
			$dest = imagecreatetruecolor($w,$h);
			imagecopy($dest, $src, 0, 0, $x, $y, $w,$h);
			header('Content-Type: image/jpeg');
			imagejpeg($dest,$newPath);
		}else if ( strtolower($pathinfo['extension']) =="png" ){
			//PNG handling
			$src = imagecreatefrompng($localimgpath);
			$dest = imagecreatetruecolor($w,$h);
			imagecopy($dest, $src, 0, 0, $x, $y, $w,$h);
			header('Content-Type: image/jpg');
			imagejpeg($dest,$newPath);
		}else if ( strtolower($pathinfo['extension']) =="gif" ){
			//GIF handling - untested
			$src = imagecreatefromgif($localimgpath);
			$dest = imagecreatetruecolor($w,$h);
			imagecopy($dest, $src, 0, 0, $x, $y, $w,$h);
			header('Content-Type: image/jpg');
			imagejpeg($dest,$newPath);
		}
		imagedestroy($dest);
		imagedestroy($src);
		return file_exists($newPath);
	}

	function get_items( $image_id ){
		$query = "SELECT * FROM  `".self::TABLENAME."`
		WHERE 
			`image_id`=".$this->customdatabase->escape($image_id)."
		";
		return $this->customdatabase->query( $query );
	}



	public function getItemsDetailsByStorageId( $storage_id ){
		$query = "SELECT 
			i.*,
			images.filename
			FROM  
				`".self::TABLENAME."` i
			INNER JOIN 
				`images` on (images.id = i.image_id)
			

		WHERE 
			i.`storage_id`=".$this->customdatabase->escape($storage_id)."
		ORDER BY 
			i.category_id
		";

		$res = $this->customdatabase->query($query);
		foreach ( $res as $k=>$v ){
			$res[$k]['img_path'] = base_url( self::ITEM_FOLDER . $this->hash($v['id']).".jpg" );
		}
		return $res;

	}


}