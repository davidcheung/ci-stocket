<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class category extends CI_Model {

	const TABLENAME = "categories";

	function __construct(){
	}

	function getCategories(){
		$query = "SELECT 
			* 
		FROM  `".self::TABLENAME."`
		WHERE 
			1
		ORDER BY `sortorder`
		";
		return $this->customdatabase->query( $query );
	}

}
?>