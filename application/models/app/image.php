<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class image extends CI_Model {

	public $storage_id;
	public $storage_label;
	public $storage_userid;
	protected $data;
	const TABLENAME = "images";
	const IMAGE_PATH = "uploads/";
	protected $hashObj;

	public function __construct(){
		$this->hashObj = new Hashids('images',15,'abcdefghijklmnopqrstuvwxyz1234567890_');
	}

	public function hash($id){
		return $this->hashObj->encode($id);
	}

	public function unhash($str){
		$result = $this->hashObj->decode($str);
		return $result[0];
	}

	public function findImages($storage_id){
		$query = "SELECT 
			i.*,
			(SELECT count(1) from items where (items.image_id = i.id) ) as count
		 FROM 
		`".self::TABLENAME."` i
		WHERE 
			`storage_id`=".$this->customdatabase->escape($storage_id)."
		";
		$res = $this->customdatabase->query( $query );
		foreach ( $res as $k=>$v ){
			$res[$k]['img_path'] = $this->buildImagePath($storage_id, $v['filename'] );
			$res[$k]['id_hash'] = $this->hash($v['id']);
		}
		return $res;
	}

	public function buildImagePath( $storage_id, $filename, $local = false ){
		if ( $local ){
			return "uploads/" . $this->storage->hash($storage_id) . "/" . $filename;
		}else{
			return base_url( "uploads/" . $this->storage->hash($storage_id) . "/" . $filename );
		}
	}

	public function getStorageHash($storage_id){
		return $this->storage->hash($storage_id);
	}

	public function findImageDetails( $id){
		$query = "SELECT 
			i.*,
			(SELECT count(1) from items where (items.image_id = i.id) ) as count
		 FROM 
			`".self::TABLENAME."` i
		WHERE 
			`id`=".$this->customdatabase->escape($id)."
		";
		$res = $this->customdatabase->query( $query );
		$res[0]['id_hash'] = $this->hash($res[0]['id']);
		$res[0]['img_path'] = $this->buildImagePath( $res[0]['storage_id'], $res[0]['filename']);
		return $res[0];
	}

	public function createImageRecord( $storage_id, $picture_name ) {
		$query = "INSERT INTO `".self::TABLENAME."`
		SET
			`filename`=".$this->customdatabase->escape($picture_name).",
			`storage_id`=".$storage_id.",
			`added_on`='".date('Y-m-d H:i:s')."'
		";
		$id = $this->customdatabase->insert($query);
		return $id;
	}

	public function bulkUpload( $storage_id, $files ){
		foreach ( $files['uploads']['name'] as $k=>$v ){
			$picture_name = $files["uploads"]["name"][$k];
			$folder = self::IMAGE_PATH . $this->storage->hash($storage_id) ."/";
			$path = $folder . $picture_name;
			$temp = self::IMAGE_PATH . "temp/". $picture_name;
			//check if folder exist
			if ( !file_exists( $folder )){
				mkdir($folder);
				chmod($folder  , 0755);
			}
			//if successfully save copy image from tmp -> uploads
			if ( move_uploaded_file( $files["uploads"]["tmp_name"][$k],
  			$temp ) ){
				//create record in database
				//copy($temp, $path );
				$this->createthumb( $temp, $path, 720,720 );
				if ( file_exists ( $path ) ){
					unlink( $temp );
					$imageids[] = $this->createImageRecord( $storage_id, $picture_name );
				}

  			}
		}
		return $imageids;
	}

	function getExtension($str) {
	    $ext = pathinfo($str, PATHINFO_EXTENSION);
	    return $ext;
	}

	function compress_image($source_url, $destination_url, $quality = 70) {
    	$info = getimagesize($source_url);
	    if ($info['mime'] == 'image/jpeg'){$image = imagecreatefromjpeg($source_url);}
	    else if ($info['mime'] == 'image/gif'){$image = imagecreatefromgif($source_url);}
	    else if ($info['mime'] == 'image/png'){$image = imagecreatefrompng($source_url);}
	    imagejpeg($image, $destination_url, $quality);
	    return $destination_url;
	}


	function createthumb($name,$filename,$new_w,$new_h){
		$system=explode(".",$name);
		$ind=count($system) - 1;
		$isize=getimagesize($name);
		$iwidth=$isize[0];
		$iheight=$isize[1];
		/* -----------------------------------------------------*/
		/* added so small images doesnt create a huge thumbnail */
			if ($iwidth<=$new_w && $iheight<=$new_h)
			{
					$new_w=$iwidth;
					$new_h=$iheight;
			}
		/* -----------------------------------------------------*/
		if (preg_match("/png/",strtolower($system[$ind]))){
			$src_img=imagecreatefrompng($name);
		}
		else if (preg_match("/gif/",strtolower($system[$ind]))){
			$src_img=imagecreatefromgif($name);
		}
		else if (preg_match("/bmp/",strtolower($system[$ind]))){
			$src_img=imagecreatefromwbmp($name);
		}
		else {
			$src_img=imagecreatefromjpeg($name);
		}
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		if ($old_x > $old_y){
			$thumb_w=$new_w;
			$thumb_h=$old_y*($new_w/$old_x);
		}
		if ($old_x < $old_y){
			$thumb_w=$old_x*($new_h/$old_y);
			$thumb_h=$new_h;
		}
		if ($old_x == $old_y){
			$thumb_w=$new_w;
			$thumb_h=$new_h;
		}

		$dst_img=imagecreatetruecolor($thumb_w,$thumb_h);
		imagesavealpha($dst_img, true);
		$trans_colour = imagecolorallocatealpha($dst_img, 0, 0, 0, 127);
	    imagefill($dst_img, 0, 0, $trans_colour);
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
		if (preg_match("/png/",strtolower($system[$ind])))
		{
			imagepng($dst_img,$filename);
		}
		else if (preg_match("/gif/",strtolower($system[$ind])))
		{
			//createthumbforgif($name,$filename,$thumb_w,$thumb_h);
			imagegif($dst_img,$filename);
		}
		else if (preg_match("/bmp/",strtolower($system[$ind])))
		{
			imagewbmp($dst_img,$filename);
		}
		else {
			imagejpeg($dst_img,$filename);
		}
		imagedestroy($dst_img);
		imagedestroy($src_img);
		return true;
	}

}
?>