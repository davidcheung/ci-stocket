<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CustomDatabase{

	private $ci;
	protected $connection;
	static protected $db;
	private $dbconfig;
	public $connected = false;

	public function __construct(){
		$this->ci =& get_instance();
		if ( !$this->connected ){
			$this->readConfig();
			$this->db = $this->connect();
		}
	}

	private function readConfig(){
		include(APPPATH . "config/database.php");
		$this->dbconfig['hostname']  = $db['default']['hostname'];
		$this->dbconfig['username']  = $db['default']['username'];
		$this->dbconfig['password']  = $db['default']['password'];
		$this->dbconfig['database']  = $db['default']['database'];
	}

	public function connect(){
		$this->db = new PDO(
			'mysql:host='.$this->dbconfig['hostname'].
			';dbname='.$this->dbconfig['database'].
			';charset=utf8',
			$this->dbconfig['username'],
			$this->dbconfig['password']
		);
		return $this->db;
	}

	public function query($query){
		$res = $this->db->query($query);
		return $res->fetchAll(PDO::FETCH_ASSOC);
	}

	public function insert($query){
		$res = $this->db->exec($query);
		return $this->db->lastInsertId();
	}

	public function update($query){
		$update = $this->db->prepare($query);
		$update->execute();
		return $update->rowCount();
	}

	public function escape($str){
		return $this->db->quote($str);
	}

	public function delete(){

	}

	public static function fieldsMapping( $map = array(), $input = array() ){
		$result = array();
        //this makes it usable for update and doesnt overwrite fields not passed
        foreach ( $map as $k=>$v ){
            if ( isset($input[$v]) ){
                $result[$k] = $input[$v];
            }
        }
        return $result;
	}

	public static function array2querystring( $array = array() , $implodeWith =",", $equals = "=", $wildcard="" ){
		/* ---------------------------------------------------------------
			array(
				"field-1"=>"value-1",
				"field-2"=>"value-2" 
			);

			Will turn into 

			`field-1` = 'value-1' , `field-2` = 'value-2' ...

		--------------------------------------------------------------- */
		$conditions = array();
		foreach ($array as $key => $value) {
			if (  is_array ( $value ) ){
				$conditions[] =  "`$key` $equals '$wildcard".mysql_real_escape_string( json_encode( $value ) )."$wildcard'";
			}
			else{
				$conditions[] =  "`$key` $equals '$wildcard".mysql_real_escape_string(  stripslashes($value)  )."$wildcard'";	
			}
		}
		return implode( $implodeWith , $conditions );
	}
}
?>