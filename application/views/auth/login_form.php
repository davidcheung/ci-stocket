<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stocket</title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url('assets/css/stocket.css')?>">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, user-scalable=no">
<style>

</style>
<body class="login-body">
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30,
	'class' => 'note'
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);
?>
<?php
	function bootstrapErrorMsg($msg){
		if ( $msg ){
			return '<div class="alert alert-danger alert-dismissible" role="alert">' .
			'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.
			$msg.
			'</div>';
		}else{
			return '';
		}
	}


	echo isset($login['name'])?bootstrapErrorMsg(form_error($login['name'])) : "";
	echo isset($password['name']) ? bootstrapErrorMsg(form_error($password['name'])) : "";
	if ( isset( $errors[$login['name']]) ) {
		echo bootstrapErrorMsg($errors[$login['name']]);
	}
	if ( isset($errors[$password['name']]) ){
		echo bootstrapErrorMsg($errors[$password['name']]);
	}

?>


<div style="text-align:center">
		<div class="login-page-center-box">

			<div class="heading">
				Welcome to
			</div>
			<div class="login-main-logo">
				<img src="<?=base_url('assets/images/assets/logo.png')?>" alt="Stocket">
			</div>
			<div class="heading">
				The App that takes grocery<br/> stock from your pictures
			</div>
		
			<?php echo form_open($this->uri->uri_string()); ?>

				<!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
				<input style="display:none" type="text" name="chromeignoringautofill"/>
				<input style="display:none" type="password" name="chromeignoringautofill"/>

				<div class="login-panel login-form-group">
					<div class="username" >
						<input class="note" type="email" name="login" value="<?=set_value('login')?>" id="login" maxlength="80" size="30" placeholder="Email Address">
					</div>
					<hr/>
					<div class="password">
						<?php echo form_password($password,"","placeholder=\"Password\""); ?>
					</div>
				</div>

				<div class="login-form-group login-remember">
					<label>
						<?php echo form_checkbox($remember); ?>
						<span><?php echo form_label('Remember me', $remember['id']); ?></span>
					</label>
				</div>

				<input class="login-button login-form-group" type="submit" value="LET'S TAKE STOCK">
				<br/>

				<div class="note">I don't have an account yet. 
					<?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Create One'); ?>
				</div>
				<div class="note">
					<?php echo anchor('/auth/forgot_password/', 'Forgot password'); ?>
				</div>
			</form>

		</div>
</div>

</body>
</html>