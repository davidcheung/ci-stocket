<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stocket</title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url('assets/css/stocket.css')?>">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<meta name="viewport" content="width=device-width, user-scalable=no">
<style>
</style>
<body class="login-body">
<?php

/* ---------------------
The input and their attribute + styles 
--------------------- */
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
		'size'	=> 30,
	);
}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder' => 'Email Address',
	'autocomplete' => 'off'
);
$firstname = array(
	'name'	=> 'firstname',
	'id'	=> 'firstname',
	'value'	=> set_value('firstname'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder' => 'First Name',
	'autocomplete' => 'off'
);
$lastname = array(
	'name'	=> 'lastname',
	'id'	=> 'lastname',
	'value'	=> set_value('lastname'),
	'maxlength'	=> 80,
	'size'	=> 30,
	'placeholder' => 'Last Name',
	'autocomplete' => 'off'
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' => 'Password',
	'autocomplete' => 'off'
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
	'size'	=> 30,
	'placeholder' => 'Confirm Password',
	'autocomplete' => 'off'

);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);

/* -----------------------------
Error handling 
----------------------------- */
function bootstrapErrorMsg($msg){
	if ( $msg ){
		return '<div class="alert alert-danger alert-dismissible" role="alert">' .
		'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.
		$msg.
		'</div>';
	}else{
		return '';
	}
}
//echo "<pre>". print_r ( $_REQUEST , true ) . "</pre>";
if ( isset( $_POST['email'] ) &&  !trim($_POST['email'] ) ){
	echo bootstrapErrorMsg("Email cannot be empty");
}
if ( isset( $_POST['password'] ) && !trim($_POST['password']) ){
	echo bootstrapErrorMsg("Password cannot be empty");
}
if ( isset( $_POST['confirm_password'] ) && !trim($_POST['confirm_password']) ){
	echo bootstrapErrorMsg("Confirm Password cannot be empty");
}
if ( isset($errors[$email['name']] ) ){
	echo bootstrapErrorMsg( $errors[$email['name']] );
}
echo bootstrapErrorMsg(form_error($email['name'])); 
echo bootstrapErrorMsg(form_error($password['name'])); 
echo bootstrapErrorMsg(form_error($confirm_password['name'])); 


?>

<div style="text-align:center">
	<div class="login-page-center-box">
		<div class="heading">
				Please fill in your information
		</div>
<?php echo form_open($this->uri->uri_string(), 'autocomplete="off"'); ?>
	<!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
	<input style="display:none" type="text" name="chromeignoringautofill"/>
	<input style="display:none" type="password" name="chromeignoringautofill"/>


	<div class="login-panel login-form-group">
		<div class="username" >
			<input type="email" name="email" value="<?=set_value('email')?>" id="email" maxlength="80" size="30" placeholder="Email Address">
		</div>
		<hr/>
		<div class="username">
			<?php echo form_input($firstname); ?>
		</div>
		<hr/>
		<div class="username" >
			<?php echo form_input($lastname); ?>
		</div>
		<hr/>
		<div class="password">
			<?php echo form_password($password); ?>
		</div>
		<hr/>
		<div class="password">
			<?php echo form_password($confirm_password); ?>
		</div>
	</div>
	
	<input class="login-button login-form-group" type="submit" name="Register" value="Register">

<?php echo form_close(); ?>
		</div>
</div>
</body>
</html>