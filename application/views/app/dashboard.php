<ul class="app-dashboard">
<?php
	foreach ( $storage as $k=>$v ){
		?>
		
		<li class="<?=( $v['status']==2?'action-disabled':'' )?>">
		<a href="<?=(  $v['status'] == 1 ? base_url('storages/'.$v["id_hash"]) : "javascript:void(0)")?>">
			<div class="name"><?=$v['name']?></div>
			
			<?php
			if ( $v['status'] == 2 ){
				?>
				<div class="right-side">
					<div class="action">
						Processing...
					</div>
				</div>
				<div class="date"><?=date('M j',strtotime($v['added_on']))?></div>
				<?php
			}else{
				?>
				<div class="right-side">
					<div class="action">
						<?=$v['image_count']?>
						<i class="glyphicon glyphicon-picture"></i>
						<i class="glyphicon glyphicon-chevron-right"></i>
					</div>
					<div class="count"><?=$v['item_count']?></div>
				</div>
				<div class="date"><?=date('M j',strtotime($v['added_on']))?></div>
				
				<?php
			}
			?>
			<div class="clear"></div>
		</a>
		</li>		
		<?php
	}
	
?>
</ul>

<!-- <button onclick="addStorage()">Add</button> -->
<script>
	function addStorage(){
		$.ajax({
			url : 'storages/add',
			success:function( res ){
				$("#content-area").append( res );
				$(".modal").modal();
			}
		})
	}
</script>
