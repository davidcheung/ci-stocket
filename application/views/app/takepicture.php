<form action="picture/ajaxUpload" method="post" id="upload-form" enctype="multipart/form-data" onsubmit="return false;">
	<div class="center upload-container" >
		<input type="hidden" name="test[]" value="1">
		<input type="hidden" name="test[]" value="2">
		
		


		<div class="images-preview"></div>
		<div id="progress-bar" style="display:none;" class="progress-radial">
			  <div class="overlay">0%</div>
		</div>
		
		<div class="storage-container">
			<div class="login-welcome">
				These pics were taken of:
				<div class="clear" style="padding-top:0.3em;"></div>
				<select onchange="checkAddMore(this.value)" class="app-dropdown" name="storage" id="storage-dropdown">
				<?php
					foreach ( $storage as $k=>$v ){
						$storages[] = $v['name'];
					}
					$storages = array_unique( $storages);
					echo "<option>".implode("</option><option>",$storages)."</option>";
				?><option value="">+ &nbsp;&nbsp;Add More</option>
				</select>
			</div>
			

			<div class="clear" style="padding-top:0.6em;"></div>
			<input class="submit-button" type="submit" value="Upload" onclick="ajaxSubmit();">
		</div>
		
	</div>
	<script>
	function checkAddMore(value){
		if ( !value ){
			var input = $('<input name="storage" placeholder="Fridge"/>').css({
				"width": "40%",
				"border": "none",
				"font-size": "1.3em",
				"line-height": "120%",
				"font-weight": "300",
				"padding": "1%"
			});
			$("#storage-dropdown").hide().after(input);

		}

	}</script>
	
</form>


<script>
function fullScreenOverlay(){
	if ( $(".full-screen-overlay").length < 1 ){
		var div = $('<div class="full-screen-overlay"/>');
	}else{
		var div = $(".full-screen-overlay");
	}
	$('body').prepend( div );
	return div;
}

function hasFiles(){
	var hasFiles = false;
	$('.picture-input').each( function(){
		if ( $.trim($(this).val()) != "" ){
			hasFiles = true;
		}
	});
	return hasFiles;
}
function ajaxSubmit(){
	if ( !hasFiles() ){
		alert( "No pictures selected");
		return;
	}
	var options = {
		uploadProgress: OnProgress,
		beforeSubmit : initProgressBar( "progress-bar" ),
		success:function(res){
			if ( res.result =="success"){
				var overlay = fullScreenOverlay();
				var button_container = $('<div style="text-align:center;"/>');
				var button = $('<a href="dashboard" class="button" style="margin:auto;">HOME DASHBOARD</a>');
				overlay.append( $('<div style="clear:both;padding-top:20px;"/>'), button_container.append(button) );
					$("#status-msg").text("" );

			}else{
				alert( res.msg );
			}
			
		}
	}
	$("#upload-form").ajaxSubmit(options);
	return false;
	function OnProgress(event, position, total, percentComplete)
	{
		progressbar( "progress-bar", percentComplete );
		if ( percentComplete == 100 ){
			$("#status-msg").text("Compressing Images..." );
		}
		//console.log(  percentComplete + "/" + total );
	    // //Progress bar
	    // progressbar.width(percentComplete + '%') //update progressbar percent complete
	    // statustxt.html(percentComplete + '%'); //update status text
	    // if(percentComplete>50)
	    //     {
	    //         statustxt.css('color','#fff'); //change status text to white after 50%
	    //     }
	}
}
function initProgressBar(id){
	var overlay = fullScreenOverlay();
	var header = $('<div class="beige-logo">Stocket</div>');
	

	var description = $('<div class="heading"/>')
	.html( "<b>Uploading your pics</b><br/>Please be patient, this may take a few minutes")
	.css({
		"position": "relative",
		"margin-top": "4%",
		"width": "100%",
		"height" : "auto"
	});
	var status_message = $('<div class="heading" id="status-msg">Uploading...</div>');
	overlay.prepend( header,description );
	var $element = $("#"+id);
	overlay.append( $element.show(),status_message ).show();
	progressbar( id, 0 );
}
function progressbar( id, percent ){
	var $element = $("#"+id);
	if ( percent > 100 ){
		percent = 100;
	}
	if ( percent  < 50 ){
		var left_deg = 90;
		var right_deg = 90+percent*3.6;
		$element.css( {
			"background-image" : "linear-gradient("+left_deg+"deg, #B1C12A 50%, transparent 50%, transparent), linear-gradient("+right_deg+"deg, #E27E30 50%, #B1C12A 50%, #B1C12A)"
		});
	}else{
		var right_deg = (-90)+(percent-50)*3.6;
		var left_deg = 270;
		$element.css( {
			"background-image" : "linear-gradient("+left_deg+"deg, #E27E30 50%, transparent 50%, transparent), linear-gradient("+right_deg+"deg, #E27E30 50%, #B1C12A 50%, #B1C12A)"
		});
	}
	$("#"+id+" .overlay").text(percent+"%");
}

var myfiles = [];
$(function(){
	
	function newFileContainer(){
		var container = $(".upload-container");
		var picturecontainer = $('<div class="picture-container"/>');
		var input = $('<input class="picture-input" type="file" name="uploads[]" capture="camera" accept="image/*" multiple/>');
		var span = $('<span class="action">Add Picture <i  class="glyphicon glyphicon-camera"></i> </span>');
		span.click( function(){ input.click() })
		container.prepend( picturecontainer.append(span, input ));
		input.on("change", function(event){ gotPic(event,span) } );
	}

	newFileContainer();

	function gotPic(event,input) {
		//console.log( event.target );
        if(event.target.files.length > 0 ){
        	for (var i = 0; i < event.target.files.length; i++) {
        		if ( event.target.files[i].type.indexOf("image/") == 0 ){
        			var img = $('<img/>')
        				.attr('src',URL.createObjectURL(event.target.files[i]))
        				.addClass('camera-preview');
        			$(".images-preview").append(img);
        			myfiles.push( event.target.files[i] );
        		}
        	};
        	input.hide();
        	newFileContainer();
        }
	}
});
</script>
