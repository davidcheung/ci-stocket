<!-- <pre><?=print_r( $categories, true)?><?=print_r( $items, true)?>
</pre> -->

<button class="add-btn">
<i class="glyphicon glyphicon-plus"></i>
Add Item
</button>	
<?php
	foreach ( $categories as $k=>$v ) {
		//very inefficiently putting items into category
		$categories[$k]['items'] = array();
		foreach ( $items as $key=>$item ){
			if ( $item['category_id'] == $v['id'] ){
				$categories[$k]['items'][] = $item;

			}
		}
	}
?>
<ul class="item-list">
	<?php
		foreach ( $categories as $k=>$v ){
			if ( count ( $v['items']) > 0 ){
			?>
			<li>
				<div class="category-title">
					<?=$v['category']?>
					<span class="handle">
						<i class="glyphicon glyphicon-chevron-up"></i>
					</span>
				</div>
				<div class="category-items">
					<ul class="item-list-items">
						<?php 
						foreach ( $v['items'] as $key=>$item ){
						 	?>
						 		<li>
						 			<div class="item-icon">
						 				<img src="<?=$item['img_path']?>" alt="">
						 			</div>
						 			<div class="item-name-qty-block">
						 				<div class="item-name"><?=$item['name']?></div>
						 				<div class="item-qty"><?=$item['quantity']?></div>
						 			</div>
						 			
						 			<div class="item-edit">
						 				<i class="glyphicon glyphicon-edit"></i>
						 			</div>
						 			<div class="clear"/>
						 		</li>
						 	<?php
						}
						?>
					</ul>
				</div>
			</li>
			<?php
		}
		}
	?>
	
</ul>

<style>
/* -- TO BATTLE HTTP SERVER CACHE --- */
.item-name-qty-block{
	float: left;
	width: 65%;
	text-overflow: ellipsis;
}
</style>