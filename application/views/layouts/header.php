<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Stocket</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, user-scalable=no">

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url('assets/css/stocket.css')?>">
	<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url('assets/js/jquery.form.js')?>"></script>
    
    
</head>
<body class="app">
	<div class="navbar navbar-inverse navbar-stocket navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header pull-left">
            <a class="navbar-brand" href="<?=base_url()?>">Stocket</a>
        </div>
        <div class="navbar-header pull-right">

          	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
	     	</button>
          
        </div>
        <div class="navbar-header pull-right nav-constant">
            <ul class="nav navbar-nav">
                <li class="<?=($current_page=='dashboard'?"active":"")?>"><?=anchor('dashboard','<i class="glyphicon glyphicon-home"></i>')?></li>
            </ul>
        </div>
        <div class="navbar-header pull-right nav-constant">
            <ul class="nav navbar-nav ">
                <li class="<?=($current_page=='picture'?"active":"")?>"><?=anchor('picture','<i class="glyphicon glyphicon-camera"></i>')?></li>
            </ul>
        </div>


        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="<?=($current_page=='dashboard'?"active":"")?>">
                <?=anchor('dashboard','Dashboard')?></li>
            <?php 
                if( $profile['is_admin'] == 1 ){
                    ?>
                    <li class="<?=($current_page=='admin'?"active":"")?>">
                <?=anchor('admin','Admin')?></li>
                    <?php
                }
            ?>
            <li>
                <?=anchor('auth/logout',"Logout")?>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
       
      </div>
    </div>
    <div class="container" id="content-area">
        <!--- Content goes in here -->
