<div style="margin:auto;" >
	Storage Status : 
	<select onchange="updateStatus(this.value)"> 
	 	<option value="1" <?=($storage['status'] == 1? "selected" : "")?>>Active</option>
	 	<option value="2" <?=($storage['status'] == 2? "selected" : "")?>>Pending</option>
	</select>
</div>
<hr class="normal"/>

 <ul class="image-list">
	<?php 
	foreach ( $images as $k=>$v ){
		?>
		<li>
			<!-- <div>
				<span class="glyphicon glyphicon-picture"></span>
				<?=$v['id']?>
			</div> -->
			<div class="img-div pull-left">
				<a target="_blank" href="<?=base_url( "admin/image/". $v['id_hash'] )?>">
					<img src="<?=$v['img_path']?>" alt="">
				</a>
			</div>
			<div class="pull-right right-panel">
				<ul>
					<li>
						Image # : <span class="glyphicon glyphicon-picture"></span> <b><?=$v['id_hash']?></b>
					</li>
					<li>Items : <span class="count"><?=$v['count']?></span></li>
				</ul>
			</div>
			<div class="clear"></div>
		</li>
		<?php
	}
	?>

 </ul>

<script>
function updateStatus( new_status ){
	if( !new_status ){
		alert("Error");
		return;
	}
	//alert(new_status); return;
	$.ajax({
		url : "<?=base_url('adminajax/change_storage_status')?>",
		type : "POST",
		data : { storage_id : '<?=$storage['id']?>', status : new_status },
		success:function( res ){
			if( res.data == 0){
				alert( "Nothing changed");
			}else if( res.data == 1){
				alert( "Status updated");
			}
		}

	})
}
</script>