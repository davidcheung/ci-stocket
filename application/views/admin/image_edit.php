<pre>
<?=print_r($image,true)?>
</pre>


<div class="img-container">
	<img id="img" src="<?=$image['img_path']?>" alt="">
</div>

<div id="item-form-container" class="login-panel">
	<form id="item-form" action="<?=base_url('adminajax/add_item')?>" method="post" onsubmit="return false;">
		<input type="hidden" name="storage_id" value="<?=$image['storage_id']?>">
		<input type="hidden" name="image_id" value="<?=$image['id']?>">
		<input id="item-name" name="item-name" type="text" placeholder="Name"> <hr/>
		<select name="category_id" Placeholder="Category" id="category-select">
			<option value=""  disabled selected style='display:none;'>Select a Category</option>
			<?php foreach( $categories as $k=>$v ){
				?><option value="<?=$v['id']?>"><?=$v['category']?></option><?php
			}
			
			?>
		</select>
		<!-- <input id="item-cat" name="item-cat" type="text" placeholder="Category">  --><hr/>
		<input id="item-qty" name="item-qty" type="text" placeholder="Quantity">
		<hr/>
		x : <input id="coordinates-x" name="coordinates-x" class="coordinates" type="text">
		y : <input id="coordinates-y" name="coordinates-y" class="coordinates" type="text">
		w : <input id="coordinates-w" name="coordinates-w" class="coordinates" type="text">
		h : <input id="coordinates-h" name="coordinates-h" class="coordinates" type="text">
		<hr/>
		<button class="submit-button small-btn" onclick="submitAddItem()">Add</button>
	</form>
</div>



<link rel="stylesheet" href="<?=base_url('assets/jcrop/css/jquery.Jcrop.min.css')?>">
<script src="<?=base_url('assets/jcrop/js/jquery.color.js')?>"></script>
<script src="<?=base_url('assets/jcrop/js/jquery.Jcrop.min.js')?>"></script>

<script>
	var jcrop_api;
	jQuery(function($){

		$('#img').Jcrop({
			aspectRatio: 1,
			onSelect : function(c ){
				console.log( c );
				addItemForm( c );
			}
		},function(){
			jcrop_api = this;
		});
		getItems();

	});

	function submitAddItem(){
		$("#item-form").ajaxSubmit({
			success:function(res){
				if ( parseInt(res.data.res) > 0 ){
					alert( "Item created ");
					formReset();
					$(".each-item").remove();
					getItems();
				}
				
			}
		});
	}

	function formReset(){
		//clearing form 
		$("#item-name").val("");
		$("#item-qty").val("");
		$("#category-select option:first").attr("selected","selected");
		$("input.coordinates").val("");
	}
	function addItemForm( c ){
		console.log(c.x)
		$("#coordinates-x").val(c.x);
		$("#coordinates-y").val(c.y);
		$("#coordinates-w").val(c.w);
		$("#coordinates-h").val(c.h);	
	}
	
	function getItems(){
		$.ajax({
			url : "<?=base_url('adminajax/get_items')?>",
			type : 'POST',
			data : { image_id : '<?=$image['id']?>' },
			success:function( result ){
				for ( var i = 0; i < result.data.res.length; i++){
					selectedImage(result.data.res[i]);
				}
			}
		})
	}

	function selectedImage( res ){
		var item = $('<div class="each-item"/>').css({
			width : res.w,
			height: res.h,
			top : res.y+'px',
			left : res.x+'px'
		});
		item.click( function(){
			jcrop_api.animateTo([ 
				res.x,
				res.y,
				parseInt(res.w) + parseInt(res.x),
				parseInt(res.h) + parseInt(res.y)
			])
		})
		$(".img-container").prepend(item);
	}


</script>
<style>
	#item-form-container{
		font-size:75%;
		width: 40%;
		padding:15px;
		border:1px solid #c3c3c3;

	}
	.img-container{
		position:relative;
	}
	#img{
		position:relative;
	}
	.each-item{
		position:absolute;
		border:yellow 1px solid;
		z-index:380;
	}
	.each-item:hover{
		border-color:orange;
	}
	input.coordinates{
		width: 40px;
		padding: 0;
		line-height: 150%;
		font-size: 1.3em;
		margin: 5px;
		text-align: center;
		background : lightgrey;

	}

</style>