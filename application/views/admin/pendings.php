<!-- <pre>
<?=print_r($storage,true)?>
</pre> -->
<ul class="app-dashboard">
<?php
	foreach ( $storage as $k=>$v ){
		if ( $v['status'] == 2){
			$status_msg = "Pending";
		}else if ( $v['status'] == 1){
			$status_msg =  "Active";
		}else{
			$status_msg =  "Deleted";
		}
		?>
		
		<li class="">
		<a href="<?=base_url('admin/storages/'.$v["id_hash"])?>">
			<div class="status">[<?=$status_msg?>]</div>
			<div class="name"><?=$v['name']?></div>
			<div class="right-side">
				<div class="action">
					<?=$v['image_count']?>
					<i class="glyphicon glyphicon-picture"></i>
					<i class="glyphicon glyphicon-chevron-right"></i>
				</div>
			</div>
			<!-- <div class="date"><?=date('M j g:i:a',strtotime($v['added_on']))?></div> -->
			<div class="clear" style="border-bottom:1px dashed #c3c3c3;"></div>
			<div class="extra-info">
				Created By
				<i class="glyphicon glyphicon-user"></i>
				<?=ucwords($v['firstname'] . " " . $v['lastname'])?> on 
				<i class="glyphicon glyphicon-calendar"></i>
				<?=date('M j g:i:a',strtotime($v['added_on']))?>
			</div>
		</a>
		</li>
		
		<?php
	}
	
?>
</ul>
